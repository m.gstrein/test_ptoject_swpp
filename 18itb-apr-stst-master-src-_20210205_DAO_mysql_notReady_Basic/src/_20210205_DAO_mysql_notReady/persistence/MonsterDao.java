package _20210205_DAO_mysql_notReady.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import _20210205_DAO_mysql_notReady.persistence.models.Monster;

public class MonsterDao implements Dao<Monster> {
    
    public MonsterDao() {
        
        System.out.println("Connecting ...");
        DbControllerMonster.connect();
        System.out.println("Creating Table ...");
        DbControllerMonster.createMonsterTable();        

        this.save(new Monster("John", "john@domain.com"));
        this.save(new Monster("Susan", "susan@domain.com"));
    }
    
    @Override
    public Optional<Monster> get(int id) {
        return Optional.ofNullable(new Monster(DbControllerMonster.getName(id), DbControllerMonster.getEmail(id)));
    }
    
    @Override
    public List<Monster> getAll() {
        return DbControllerMonster.getAllMonster();
    }
    
    @Override
    public void save(Monster monster) {
        DbControllerMonster.insertMonster(monster.getName(), monster.getEmail());
    }
    
    @Override
    public void update(Monster monster, String[] params) {

        DbControllerMonster.updateMonster(monster, params);
    }
    
    @Override
    public void delete(Monster monster) {
        DbControllerMonster.delete(monster);
    }

    @Override
    public void close() {
        DbControllerMonster.close();
    }

}

class DbControllerMonster {
    static Connection conn;

    public static void updateMonster(Monster monster, String[] params)
    {
        String sql = "UPDATE monsters SET name = ? , "
                + "email = ? "
                + "WHERE name = ?";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            // set the corresponding param
            pstmt.setString(1, Objects.requireNonNull(params[0]));
            pstmt.setString(2, Objects.requireNonNull(params[1]));
            pstmt.setString(3, monster.getName());
            // update 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static ArrayList<Monster> getAllMonster(){
        ArrayList<Monster> monsters = new ArrayList<>();
        String query = "SELECT name, email FROM monsters";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()){
                monsters.add(new Monster(rs.getString("name"), rs.getString("email")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return monsters;
    }

    public static String getName(int id){
        String query = "SELECT name FROM monsters WHERE id = ?";
        try{
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
            return rs.getString("name");
            }
        } 
        catch (SQLException e) {
                e.printStackTrace();
            }
        return query;
    }   

    public static String getEmail(int id){
        String query = "SELECT email FROM monsters WHERE id = ?";
        try{
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
            return rs.getString("email");
            }
        } 
        catch (SQLException e) {
                e.printStackTrace();
            }
        return query;
        }

        public static void delete(Monster monster) {
    
            String sql = "DELETE FROM monsters WHERE name = ?";
    
            try{
                PreparedStatement pstmt = conn.prepareStatement(sql);
                pstmt.setString(1, monster.getName());
                pstmt.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

    public static void insertMonster(String name, String email){
        String sql = "INSERT INTO monsters(name,email) VALUES(?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, name);
            pstmt.setString(2, email);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void createMonsterTable() {
        String sql = "CREATE TABLE IF NOT EXISTS monsters (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text NOT NULL,\n"
                + "	email text\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            System.out.println("Table warehouse is created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void connect() {
        String url = "jdbc:sqlite:./src/_20210205_DAO_mysql_notReady/mydb.db";
        try {
            conn = DriverManager.getConnection(url);            
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            e.printStackTrace();
        }         
    }

    public static void close(){
        try{
            conn.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }    
    }
}