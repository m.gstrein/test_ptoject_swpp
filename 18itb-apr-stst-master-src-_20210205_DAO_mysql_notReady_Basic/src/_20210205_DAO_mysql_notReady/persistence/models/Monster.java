package _20210205_DAO_mysql_notReady.persistence.models;

public class Monster {
    
    private String name;
    private String email;

    public Monster(String name,String email){
        this.name = name;
        this.email = email;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getEmail(){
        return email;
    }

    public String toString(){
        return getName()+" - "+getEmail();
    }

}
