package _20210205_DAO_mysql_notReady.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import _20210205_DAO_mysql_notReady.persistence.models.User;

public class UserDao implements Dao<User> {
    
    public UserDao() {
        
        System.out.println("Connecting ...");
        DbController.connect();
        System.out.println("Creating Table ...");
        DbController.createUserTable();        

        this.save(new User("John", "john@domain.com"));
        this.save(new User("Susan", "susan@domain.com"));
    }
    
    @Override
    public Optional<User> get(int id) {
        return Optional.ofNullable(new User(DbController.getName(id), DbController.getEmail(id)));
    }
    
    @Override
    public List<User> getAll() {
        return DbController.getAllUsers();
    }
    
    @Override
    public void save(User user) {
        DbController.insertUser(user.getName(), user.getEmail());
    }
    
    @Override
    public void update(User user, String[] params) {

        DbController.updateUser(user, params);
    }
    
    @Override
    public void delete(User user) {
        DbController.delete(user);
    }

    @Override
    public void close() {
        DbController.close();
    }
}

class DbController {
    static Connection conn;

    public static void updateUser(User user, String[] params)
    {
        String sql = "UPDATE users SET name = ? , "
                + "email = ? "
                + "WHERE name = ?";

        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            // set the corresponding param
            pstmt.setString(1, Objects.requireNonNull(params[0]));
            pstmt.setString(2, Objects.requireNonNull(params[1]));
            pstmt.setString(3, user.getName());
            // update 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static ArrayList<User> getAllUsers(){
        ArrayList<User> users = new ArrayList<>();
        String query = "SELECT name, email FROM users";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()){
                users.add(new User(rs.getString("name"), rs.getString("email")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public static String getName(int id){
        String query = "SELECT name FROM users WHERE id = ?";
        try{
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
            return rs.getString("name");
            }
        } 
        catch (SQLException e) {
                e.printStackTrace();
            }
        return query;
    }   

    public static String getEmail(int id){
        String query = "SELECT email FROM users WHERE id = ?";
        try{
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
            return rs.getString("email");
            }
        } 
        catch (SQLException e) {
                e.printStackTrace();
            }
        return query;
        }

        public static void delete(User user) {
    
            String sql = "DELETE FROM users WHERE name = ?";
    
            try{
                PreparedStatement pstmt = conn.prepareStatement(sql);
                pstmt.setString(1, user.getName());
                pstmt.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }

    public static void insertUser(String name, String email){
        String sql = "INSERT INTO users(name,email) VALUES(?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, name);
            pstmt.setString(2, email);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void createUserTable() {
        String sql = "CREATE TABLE IF NOT EXISTS users (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text NOT NULL,\n"
                + "	email text\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            System.out.println("Table warehouse is created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void connect() {
        String url = "jdbc:sqlite:./src/_20210205_DAO_mysql_notReady/mydb.db";
        try {
            conn = DriverManager.getConnection(url);            
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            e.printStackTrace();
        }         
    }

    public static void close(){
        try{
            conn.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }    
    }
}