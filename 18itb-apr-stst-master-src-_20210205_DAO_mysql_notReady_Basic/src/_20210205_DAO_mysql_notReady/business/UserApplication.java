package _20210205_DAO_mysql_notReady.business;

import java.util.Optional;

import _20210205_DAO_mysql_notReady.persistence.Dao;
import _20210205_DAO_mysql_notReady.persistence.DaoFacotry;
import _20210205_DAO_mysql_notReady.persistence.DaoType;
import _20210205_DAO_mysql_notReady.persistence.models.Monster;
import _20210205_DAO_mysql_notReady.persistence.models.User;

public class UserApplication {

    private static Dao<User> userDao;
    private static Dao<Monster> monsterDao;

    public static void main(String[] args) {
        DaoFacotry daoFactory = new DaoFacotry();

        userDao = daoFactory.getDao(DaoType.USER);
        
        User user1 = getUser(1);
        System.out.println("Erste Ausgabe: " + user1);
        userDao.update(user1, new String[]{"Jake", "jake@domain.com"});
        
        User user2 = getUser(2);
        userDao.delete(user2);
        userDao.save(new User("Julie", "julie@domain.com"));
        
        userDao.getAll().forEach(user -> System.out.println(user));;
        userDao.close();

        monsterDao = daoFactory.getDao(DaoType.MONSTER);
        
        Monster monster1 = getMonster(1);
        System.out.println("Erste Ausgabe: " + monster1);
        monsterDao.update(monster1, new String[]{"Jake", "jake@domain.com"});
        
        Monster monster2 = getMonster(2);
        monsterDao.delete(monster2);
        monsterDao.save(new Monster("Julie", "julie@domain.com"));
        
        monsterDao.getAll().forEach(user -> System.out.println(user));
        monsterDao.close();
    }

    private static User getUser(int id) {
        Optional<User> user = userDao.get(id);
        
        return user.orElse(
          new User("non-existing user", "no-email"));
    }

    private static Monster getMonster(int id) {
        Optional<Monster> monster = monsterDao.get(id);
        
        return monster.orElse(
          new Monster("non-existing monster", "no-email"));
    }
}