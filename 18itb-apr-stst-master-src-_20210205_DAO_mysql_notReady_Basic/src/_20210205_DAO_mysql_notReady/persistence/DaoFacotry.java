package _20210205_DAO_mysql_notReady.persistence;

public  class DaoFacotry {
    public static Dao getDao(DaoType daoType){
        switch(daoType){
            case MONSTER:
                return new MonsterDao();
            case USER:
                return new UserDao();
            default:
                return null;
        }
    }
}